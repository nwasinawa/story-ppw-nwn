from django.http import HttpResponse
from django.shortcuts import render


def home(request):
    return render(request, 'home.html', {'home':True})

def stickers(request):
    return render(request, 'stickers.html')
