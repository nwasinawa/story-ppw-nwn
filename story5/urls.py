from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.matkul, name='matkul'),
    path('details/<int:id>', views.details, name='details'),
    path('add/', views.addMatkul, name='add'),
    path('delete/<int:id>', views.delMatkul, name='del')
]