from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.contrib import messages
from datetime import datetime

from .models import Matkul, Tugas
from .forms import Form


# Create your views here.
def matkul(request):
    matkul = Matkul.objects.all()

    return render(request, 'story5/matkul.html', {'matkul':matkul})

def details(request, id): 
    matkul = Matkul.objects.get(id=id)
    tugas = Tugas.objects.all().filter(matkul=matkul)

    context = {
        'matkul':matkul, 
        'del':False, 
        'tugas':tugas, 
    }    

    # tugas and deadlines
    tugasDL = []
    for matkul in context['tugas']:
        due = matkul.dl - timezone.now()
        tugasDL.append((matkul, due.total_seconds()))
    context['deadlines'] = tugasDL        

    return render(request, 'story5/details.html', context)
 
def addMatkul(request): 
    form = Form(request.POST or None)
    if form.is_valid():
        form.save()
        form = Form()

    return render(request, 'story5/addMatkul.html', {'form':form})
 
def delMatkul(request, id):
    matkul = get_object_or_404(Matkul, id=id)
    messages.add_message(request, messages.SUCCESS, "CONFIRM DELETE: Are you sure you want to delete {}?".format(matkul))

    if request.method == 'POST':
        matkul.delete()
        messages.success(request, 'Berhasil Mengapus Mata Kuliah {}'.format(matkul.id))
        return redirect('../')
    
    return render(request, 'story5/details.html', {'matkul':matkul, 'del':True})